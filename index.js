const comOption = ["rock", "paper", "scissor"]
let isGameFinish = false;

class Janken {
    constructor() {
        if (this.constructor == Janken) {
            throw new Error("Cannot instantiate from abstract class")
        }
    }

    //com pick
    comRandomPick() {
        let maxLimit = 3;
        let randomPick = Math.random() * maxLimit;
        randomPick = Math.floor(randomPick);
        // console.log(randomPick);
        let index = randomPick;
        let comChoose = comOption[index];
        console.log(comChoose);
        return comChoose;
    }

    //add backround for player and com pick
    backgroundColorChange(userPick, comPick) {
        // player background color change
        let playerRock = document.getElementById('p-rock');
        let playerPaper = document.getElementById('p-paper');
        let playerScissor = document.getElementById('p-scissor');
        if (userPick === 'rock') {
            playerRock.classList.add('background-color-change');
        }
        if (userPick === 'paper') {
            playerPaper.classList.add('background-color-change');
        } else if (userPick === 'scissor') {
            playerScissor.classList.add('background-color-change');
        }

        // com background color change
        let comRock = document.getElementById('c-rock');
        let comPaper = document.getElementById('c-paper');
        let comScissor = document.getElementById('c-scissor');
        if (comPick === 'rock') {
            comRock.classList.add('background-color-change');
        }
        if (comPick === 'paper') {
            comPaper.classList.add('background-color-change');
        } else if (comPick === 'scissor') {
            comScissor.classList.add('background-color-change');
        }
    }

    // check who is the winner
    whoIsTheWinner(userPick, comPick) {
        if (userPick === "rock") {
            if (comPick === "rock") {
                return "draw";
            } else if (comPick === "scissor") {
                return "win";
            } else {
                return "lose";
            }
        } else if (userPick === "scissor") {
            if (comPick === "rock") {
                return "lose";
            } else if (comPick === "scissor") {
                return "draw";
            } else {
                return "win";
            }
        } else if (userPick === "paper") {
            if (comPick === "rock") {
                return "win";
            } else if (comPick === "scissor") {
                return "lose";
            } else {
                return "draw";
            }
        }
    }
    // show result in the middle
    andTheWinnerIs(result) {
        let winner = document.getElementById('winner')
        if (result === "win") {
            winner.innerHTML = "PLAYER 1 WIN"
            winner.classList.add("result")
        }
        if (result === "lose") {
            winner.innerHTML = "COM WIN"
            winner.classList.add("result")
        } else if (result === "draw") {
            winner.innerHTML = "DRAW"
            winner.classList.add("draw")
        }
    }

    // restart the game
    refresh = () => {
        isGameFinish = false;
        winner.innerHTML = "VS"
        winner.classList.remove("result")
        winner.classList.remove("draw")
        let pickWithBackground = document.getElementsByClassName('background-color-change');
        for (let i = 0; i < pickWithBackground.length; i++) {
            pickWithBackground[i].classList.remove('background-color-change');
            pickWithBackground[i].classList.remove('background-color-change');
        }
    }
}

class Player extends Janken {
    constructor() {
        super();
    }

    userPick(pick) {
        if (isGameFinish === false) {
            let comPick = super.comRandomPick();
            console.log("user pilih", pick)
            console.log("com pilih", comPick);
            super.backgroundColorChange(pick, comPick)
            let winner = super.whoIsTheWinner(pick, comPick);
            console.log(winner);
            super.andTheWinnerIs(winner);
            isGameFinish = true
        }
    }
}

class Computer extends Janken {
    constructor() {
        super();
    }
}

let Com = new Computer();
let User = new Player();
